import React, {useState} from 'react';
import './App.css';

const Header = () => <div>Header</div>;
const Footer = () => <div>Footer</div>;

function Todo() {
  const [message, setMessage] = useState();
  const [items, setItem] = useState([
    '111', '222', '333'
  ]);

  function onAddBtnClick(e) {
    console.log(message)
    setItem([...items, message]);
    setMessage('');
  }
  
  function onMessageChange(e) {
    setMessage(e.target.value)
  }

  return (
    <div>
      <ul id='todoList'>
        {items.map((item, i) => <li key={i}>{item}</li>)}
      </ul>
      <input value={message} onChange={onMessageChange} id='msgInput' type='text' placeholder="Введіть повідомлення"/>
      <button id='msgButton' onClick={onAddBtnClick}>Отправить</button>
    </div>
  )
}

function App() {
  return (
    <div className='App'>
      <Header/>
      <Todo/>
      <Footer/>
    </div>
  )
}

export default App;
