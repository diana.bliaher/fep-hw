class ContactsApi {
    static URL = 'https://62054479161670001741b708.mockapi.io/api/contacts/';

    static request(url = '', method = 'GET', body) {
        return fetch(ContactsApi.URL + url, {
            method,
            body: body ? JSON.stringify(body) : undefined,
            headers: {
                'Content-type': 'application/json',
            },
        })
        .catch((e) => {
            throw new Error(`ContactsApi can not execure request: ${e.message}`);
        });
    }

    static getList() {
        return ContactsApi
            .request()
            .then(res => {
                if (res.ok) {
                    return res.json();
                }

                throw new Error('Can not retrive todo list');
            });
    }

    static create(todo) {
        return ContactsApi
            .request('', 'POST', todo)
            .then(res => {
                if (res.ok) {
                    return res.json();
                }
        
                throw new Error('Can not create new todo');
            });
    }

    static update(id, changes) {
        return ContactsApi
            .request(id, 'PUT', changes)
            .then(res => {
                if (res.ok) {
                    return res.json();
                }
        
                throw new Error(`Can not update todo with id "${id}"`);
            });
    }

    static delete(id) {
        return ContactsApi
            .request(id, 'DELETE')
            .then(res => {
                if (res.ok) {
                    return res.json();
                }
        
                throw new Error(`Can not delete todo with id "${id}"`);
            });
    }
}

export default ContactsApi;