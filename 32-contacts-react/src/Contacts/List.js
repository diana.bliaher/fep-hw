import React from "react";
import { useDispatch } from "react-redux";
import { remove } from "../store/actions";

export default function List({contacts, onEdit}) {
  const dispatch = useDispatch();

  function deleteContact(el) {

    dispatch(remove(el.id));
  }

  function editContact(el) {
    onEdit(el);
  }

  return (
    <table>
      <tbody>
      {contacts.map( (contact) => (
        <tr key={contact.id}>
          <td>{contact.firstName}</td>
          <td>{contact.lastName}</td>
          <td>{contact.phone}</td>
          <td><button onClick={()=>editContact(contact)}>Edit</button></td>
          <td><button onClick={()=>deleteContact(contact)}>Delete</button></td>
        </tr>
      )
      )}
      </tbody>
    </table>
  )
}