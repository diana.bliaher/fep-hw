import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { sendContact } from "../store/actions";
import style from "./Contacts.module.css";

export default function Form({editContact}) {
  const dispatch = useDispatch();
  const [name, setName] = useState(editContact?.firstName ?? '');
  const [surname, setSurName] = useState(editContact?.lastName ?? '');
  const [phone, setPhone] = useState(editContact?.phone ?? '');

  function onFormSubmit(e) {
    e.preventDefault();

    const newContact = {
      ...editContact,
      firstName: name,
      lastName: surname,
      phone: phone
    };

    dispatch(sendContact(newContact));
    clearForm();
  }

  function clearForm() {
    setName('');
    setSurName('');
    setPhone('');
  }

  return (
    <form className={style.form} onSubmit={onFormSubmit}>
      <div>
        <label htmlFor="name">First name</label>
        <input id="name" value={name} onChange={ e => setName(e.target.value) } />
      </div>
      <div>
        <label htmlFor="surname">Last name</label>
        <input id="surname" value={surname} onChange={ e => setSurName(e.target.value) } />
      </div>
      <div>
        <label htmlFor="phone">Phone</label>
        <input id="phone" value={phone} onChange={ e => setPhone(e.target.value) } />
      </div>
      <button>Send</button>
    </form>
  )
}