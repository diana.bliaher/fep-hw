import React, { useEffect, useState } from "react";
import List from "./List";
import Form from "./Form";
import { useDispatch, useSelector } from "react-redux";
import { fetchList } from "../store/actions";

export default function Contacts() {
  const contacts = useSelector(state=>state);
  const dispatch = useDispatch();
  const [editContact, setEditContact] = useState();

  useEffect( () => {
    dispatch(fetchList());
  }, [])
  
  return (
    <>
      <List contacts={contacts} onEdit={setEditContact}/>
      <Form key={editContact?.id} editContact={editContact} />
    </>
  )
}