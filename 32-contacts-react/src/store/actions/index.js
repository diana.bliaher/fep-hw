import ContactsApi from "../../Contacts/ContactsApi";

export const ACTION_CONTACT_DELETE = 'delete';
export const ACTION_TODO_FETCH_LIST = 'fetchList';
export const ACTION_TODO_CREATE = 'create';
export const ACTION_TODO_REMOVE = 'remove';
export const ACTION_TODO_UPDATE = 'update';

export function fetchList() {
  return dispatch => {
    ContactsApi.getList()
    .then((list) => {
      dispatch({type: ACTION_TODO_FETCH_LIST, payload: list})
    })
  }
}

// export function create(contact) {
//   return dispatch => {
//     ContactsApi.create(contact)
//     .then((newContact) => {
//       dispatch({type: ACTION_TODO_CREATE, payload: newContact})
//     })
//   }
// }

export function remove(id) {
  return dispatch => {
    ContactsApi.delete(id)
    .then(() => {
      dispatch({type: ACTION_TODO_REMOVE, payload: id})
    })
  }
}

export function sendContact(contact) {
  return dispatch => {
    if (contact.id) {
      ContactsApi.update(contact.id, contact)
      .then((newContact) => {
        dispatch({type: ACTION_TODO_UPDATE, payload: newContact})
      })
    } else {
      ContactsApi.create(contact)
      .then((newContact) => {
        dispatch({type: ACTION_TODO_CREATE, payload: newContact})
      })
    }
  }
}