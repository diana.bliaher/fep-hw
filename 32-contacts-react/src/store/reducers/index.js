import { ACTION_TODO_CREATE, ACTION_TODO_FETCH_LIST, ACTION_TODO_REMOVE, ACTION_TODO_UPDATE } from "../actions";

export default function rootReducer(state = [], {type, payload}) {
  switch (type) {
    case ACTION_TODO_FETCH_LIST:
      return [...payload];
    case ACTION_TODO_CREATE:
      return [...state, payload];
    case ACTION_TODO_REMOVE:
      return state.filter( item => item.id != payload );
    case ACTION_TODO_UPDATE:
      return state.map( item => item.id === payload.id ? payload : item );
    default:
      return state;
  }
}