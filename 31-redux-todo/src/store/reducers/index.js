import {ACTION_TODO_CHANGE_STATUS, ACTION_TODO_DELETE} from "../actions/todo";

const INITIAL_STATE = {
  loading: false,
  todos: [
    {"title": "default 1", "status": true, "id": "1"},
    {"title": "default 2", "status": false, "id": "2"},
  ],
};

export default function rootReducer(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case ACTION_TODO_DELETE:
      return { ...state, todos: state.todos.filter(todo => todo.id !== payload) };
    case ACTION_TODO_CHANGE_STATUS:
      return { ...state, todos: state.todos.map(todo => {
        if (todo.id === payload) {
          todo.status = !todo.status;
        }
        return todo;
      }) };
    default:
      return state;
  }
}