import React from "react";
import {useDispatch} from "react-redux";
import {changeStatus, remove} from "../store/actions/todo";

export default function List({ todos, onEdit, onDelete }) {
  const dispatch = useDispatch();

  function onEditClick(e, todo) {
    e.stopPropagation();

    onEdit(todo);
  }

  function onDeleteClick(e, todo) {
    e.stopPropagation();

    dispatch(remove(todo.id));
  }

  function onTodoItemClick(e, todo) {
    e.stopPropagation();

    dispatch(changeStatus(todo.id));
  }

  return (
    <ul id="todoList">
      {todos.map((todo, i) => (
        <li
          key={todo.id}
          className={['todo-item', todo?.status ? 'done' : ''].join(' ')}
          onClick={e=>onTodoItemClick(e, todo)}
        >
          {todo.title}
          <button
            className="edit-button"
            onClick={e => onEditClick(e, todo)}
          >Edit</button>
          <button
            className="remove-button"
            onClick={e => onDeleteClick(e, todo)}
          >Delete</button>
        </li>
      ))}
    </ul>
  );
}