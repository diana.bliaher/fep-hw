'use strict';

const input = document.querySelector('#inputItem');
const list = document.querySelector('.todoList');
const btn = document.querySelector('#insertItem');

btn.addEventListener('click', listItemInsert);


function listItemInsert() {
    if (input.value != '') {
        const li = document.createElement('li');
        li.textContent = input.value;
        console.log(typeof input.value)
        list.append(li);
        input.value = '';
    }
}