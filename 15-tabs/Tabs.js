class Tabs {
    #rootEl;

    static CLASS_NAV = 'nav-btns';
    static CLASS_CONTENT = 'content-wrap';
    static CLASS_ACTIVE_BTN = 'active-btn';
    static CLASS_ACTIVE_CONTENT = 'active-content';

    constructor(rootEl) {
        this.#rootEl = rootEl;

        this.bindStyles();
        this.bindEvents();
        this.showTabContent(this.#rootEl.querySelector(`.${Tabs.CLASS_NAV} button`));
    }

    bindStyles() {
        const [navEl, contentEl] = this.#rootEl.children;

        navEl.classList.add(Tabs.CLASS_NAV);
        contentEl.classList.add(Tabs.CLASS_CONTENT);
    }

    bindEvents() {
        this.#rootEl.addEventListener('click', (e) => this.onRootElClick(e));
    }


    onRootElClick(e) {
        const tabBtn = e.target;

        if (tabBtn.closest(`.${Tabs.CLASS_NAV}`)) {
            this.showTabContent(tabBtn);
        }
    }

    showTabContent(tabBtn) {
        const btns = this.#rootEl.querySelectorAll(`.${Tabs.CLASS_NAV} button`);
        const nodesBts = Array.prototype.slice.call(btns);
        // const nodesBts = Array.from(btns);
        const contentItems = this.#rootEl.querySelector(`.${Tabs.CLASS_CONTENT}`).children;
        const indexActiveBtn = nodesBts.indexOf(tabBtn);
        const activeContentNode = contentItems[indexActiveBtn];

        this.disactiveBtns(btns)
        tabBtn.classList.add(Tabs.CLASS_ACTIVE_BTN);
        
        this.hideContent(contentItems);
        activeContentNode.classList.add(Tabs.CLASS_ACTIVE_CONTENT);
    }

    disactiveBtns(btns) {
        for (const btn of btns) {
            btn.classList.remove(Tabs.CLASS_ACTIVE_BTN);
        }
    }

    hideContent(content) {
        for (const contentItem of content) {
            contentItem.classList.remove(Tabs.CLASS_ACTIVE_CONTENT);
        }
    }
}

export default Tabs;