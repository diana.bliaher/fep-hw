class Collection {
  #list = [];

  fetch() {
    return TodoApi.getList().then((list) => {
      this.setList(list);
    })
  }

  update(id, todo) {
    const oldContact = this.contactListFind(id);

    Object.keys(todo).forEach(key => oldContact[key] = todo[key]);
    TodoApi.update(id, todo);

    return Promise.resolve();
  }

  setList(list) {
    this.#list = list;
  }

  contactListFind(id) {
    return this.#list.find(todoItem => todoItem.id === id);
  }

  getList() {
    return this.#list;
  }

  add(todo) {
    this.#list.push(todo);
  }

  find(id) {
    return this.#list.find(c => c.id === id);
  }

  delete(id) {
    this.#list = this.#list.filter(item => item.id !== id);

    TodoApi.delete(id);

    return Promise.resolve();
  }
}