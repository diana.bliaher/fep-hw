class TodoListView {
  static TODO_ITEM_SELECTOR = '.todo-item';
  static DELETE_BTN_SELECTOR = '.delete-btn';
  static DELETE_BTN_CLASS = 'delete-btn';

  #$listEl;
  #options;

  constructor(options) {
    this.#$listEl = $('<ul></ul>')
      .on('click', TodoListView.TODO_ITEM_SELECTOR, (e) => this.onTodoItemClick(e));

    this.#options = options;
  }

  onDeleteBtnClick(e) {
    e.stopPropagation();

    const id = this.getTodoItemId(e.target);

    this.#options.onDelete(id);
  }

  onTodoItemClick(e) {
    const id = this.getTodoItemId(e.target);
    const todoItem = this.getTodoItem(e.target);

    if ($(e.target).hasClass(TodoListView.DELETE_BTN_CLASS)) {
      this.onDeleteBtnClick(e)
      return;
    }
    
    const status = !$(todoItem).hasClass('done');
    const todoStatus = {
      status: status
    }
    this.#options.onUpdate(id, todoStatus);
  }

  getTodoItemId(el) {
    return el.closest(TodoListView.TODO_ITEM_SELECTOR)?.dataset.id;
  }

  getTodoItem(el) {
    return el.closest(TodoListView.TODO_ITEM_SELECTOR);
  }

  appendTo($el) {
    $el.append(this.#$listEl);
  }

  renderList(list) {
    const html = list.map(todo => this.generateTodoHTML(todo)).join('');

    this.#$listEl.html(html);
  }

  generateTodoHTML(todo) {
    const statusClass = todo.status ? 'done' : '';

    return `
      <li class="todo-item ${statusClass}" data-id="${todo.id}">
        ${todo.title}
        <span class="delete-btn">[Delete]</span>
      </li>
    `
  }
}