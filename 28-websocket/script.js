'use strict';

const form = document.querySelector('#websoket-form');
const chat = document.querySelector('#chat');
const [inputFrom, inputMessage, sendBtn] = form.children;
let socket;

initConnection();

function initConnection() {
  socket = new WebSocket('wss://fep-app.herokuapp.com');
}

socket.onopen = () => {
  sendBtn.addEventListener('click', sendMessage)
};

function sendMessage(e) {
  e.preventDefault();
  socket.send(JSON.stringify({
    username: inputFrom.value, 
    message: inputMessage.value,
  }));
  clearForm(form);
}

socket.onclose = () => {
  initConnection();
};

socket.onerror = (event) => {
  alert('Error', event.data);
};

socket.onmessage = (event) => {
  try {
    generateMessageFromTemplate(JSON.parse(event.data));
  } catch (e) {
    alert('Parse error');
  }
};

const generateMessageFromTemplate = (data) => {
  const html = messageTemplateHTML(data);
  chat.insertAdjacentHTML('beforeend', html);
} 

const messageTemplateHTML = (data) => {
  return `
    <div>${data.username}: ${data.message}</div>
  `;
}

function clearForm(form) {
  form.reset();
}