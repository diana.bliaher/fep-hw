const calculator = require('./src/calculator');

console.log(calculator.add(5, 7)) // 12
console.log(calculator.sub(12, 5)) // 7
console.log(calculator.mult(7, 4)) // 28
console.log(calculator.div(28, 2)) // 14