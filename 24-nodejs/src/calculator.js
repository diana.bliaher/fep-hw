const addFunc = require('./_add');
const subFunc = require('./_sub');
const multFunc = require('./_mult');
const divFunc = require('./_div');

module.exports = { 
  add: addFunc, 
  sub: subFunc, 
  mult: multFunc, 
  div: divFunc, 
};