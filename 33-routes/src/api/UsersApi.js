export default class UsersApi {
  static URL = 'https://jsonplaceholder.typicode.com/';

  static request(url = '') {
    return fetch(`${UsersApi.URL}${url}`)
    .then(res => {
      if (res.ok) {
        return res.json();
      }
    })
    .catch((e) => {
      throw new Error(`UsersApi can not execure request: ${e.message}`);
    });
  }
}