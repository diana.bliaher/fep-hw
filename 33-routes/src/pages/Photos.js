import React, { useEffect, useState } from 'react';
import UsersApi from '../api/UsersApi';
import { useSearchParams } from "react-router-dom";

export default function Photos () {
  const [photos, setPhotos] = useState([]);
  const [searchParams] = useSearchParams();


  useEffect(()=> {
    let albumId = searchParams.get('albumId');
    let params = `photos?albumId=${albumId}`;
    UsersApi.request(params)
    .then(setPhotos)
  }, [])

  return (
    <ul>
      {photos.map( (photo) => (
        <li key={photo.id}>{photo.title}</li>
      ) )}
    </ul>
  )
}
