import React, { useEffect, useState } from 'react';
import UsersApi from '../api/UsersApi';
import {useNavigate} from "react-router-dom";

export default function Users () {
  const [users, setUsers] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    let params = 'users';
    UsersApi.request(params)
    .then(setUsers)
  }, [])

  return (
    <ul>
      {users.map( (user) => (
        <li key={user.id}>{user.name} <button onClick={() => navigate(`/albums?userId=${user.id}`)}>Album</button></li>
      ) )}
    </ul>
  )
}