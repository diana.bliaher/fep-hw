import React, { useEffect, useState } from 'react';
import UsersApi from '../api/UsersApi';
import { useSearchParams, useNavigate } from "react-router-dom";

export default function Albums () {
  const [albums, setAlbums] = useState([]);
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();

  

  useEffect(()=> {
    let userId = searchParams.get('userId');
    let params = `albums?userId=${userId}`;
    UsersApi.request(params)
    .then(setAlbums)
  }, [])

  return (
    <ul>
      {albums.map( (album) => (
        <li key={album.id}>{album.title} <button onClick={() => navigate(`/photos?albumId=${album.id}`)}>Photos</button></li>
      ) )}
    </ul>
  )
}
