import './App.css';
import Users from './pages/Users';
import Albums from './pages/Albums';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Photos from './pages/Photos';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={< Users />} />
        <Route path="/albums" element={<Albums />} />
        <Route path="/photos" element={<Photos />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
