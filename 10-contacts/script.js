'use strict';

const contactBlock = document.querySelector('.contacts-form');
const userName = document.querySelector('#name');
const userSurName = document.querySelector('#surName');
const userPhone = document.querySelector('#phone');
const addBtn = document.querySelector('#addItem');
const deleteBtn = document.querySelector('#deleteContact');
const contactBody = document.querySelector('#contactTable tbody');
const contactTemplate = document.querySelector('#contactTemplate');

addBtn.addEventListener('click', onAddBtnClick);
contactBody.addEventListener('click', onContactBodyClick);

function onAddBtnClick() {
    const data = getMessage();
    if (!isValid(data)) {
        alert('Invalid field!');
        return;
    }
    addContactItem(data);

    clearInputs();
}

function onContactBodyClick(e) {
    if (e.target.classList.contains('delete-contact')) {
        const removeItem = getContactItem(e.target);
        removeItem.remove();
    }
}

function getContactItem(el) {
    return el.closest('tr');
}

function getMessage() {
    const message = {
        name: userName.value,
        surName: userSurName.value,
        phone: userPhone.value,
    }
    return message;
}

function isValid(data) {
    let isNameValid = /^[A-Za-zА-Яа-яЁё]+$/.test(data.name);
    let isSurNameValid = /^[A-Za-zА-Яа-яЁё]+$/.test(data.surName);
    let isPhoneValid = /^[0-9]+$/.test(data.phone) || data.phone === '';

    return isNameValid && isSurNameValid && isPhoneValid ? true : false;
};

function addContactItem(data) {
    const contactTemplateHTML = contactTemplate
        .innerHTML
        .replace('{name}', data.name)
        .replace('{surname}', data.surName)
        .replace('{phone}', data.phone);

    contactBody.insertAdjacentHTML('beforeend', contactTemplateHTML);
}

function clearInputs() {
    let inputs = document.querySelectorAll('.contacts-form input');
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].value = '';
    }
}
