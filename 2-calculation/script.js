let operand = getOperand();
let num1 = getNumber('first');
let num2 = getNumber('second');
let operandArr = ['+', '-', '*', '/'];

showAnswer(operand, num1, num2);

function getOperand() {
  return prompt('What action do you want to do (+, -, *, /)?');
}

function getNumber(numName) {
  return Number(prompt(`Enter the ${numName} number:`));
}

function showAnswer(operand, a, b) {
  let finishRes = getResult(operand, a, b)
  if (isNaN(finishRes)) {
    alert(`${finishRes}`);
  }
  else {
    alert(`Answer: ${a} ${operand} ${b} = ${finishRes}`);
  }
}

function getResult(operand, a, b) {
  let result;

  if (isNaN(a) || isNaN(b)) {
    result = 'The first number or the second number is incorrect!';
  } else if(!operandArr.includes(operand)) {
    result = 'Invalid operand';
  } else {
    switch (operand) {
      case '+':
        result = add(a, b);
        break;
    
      case '-':
        result = sub(a, b);
        break;
    
      case '*':
        result = mult(a, b);
        break;
      
      case '/':
        result = div(a, b);
        break;
    
    }
  }

  return result;
}

function add(a, b) {
  return a + b;
}

function sub(a, b) {
  return a - b;
}

function mult(a, b) {
  return a * b;
}

function div(a, b) {
  return a / b;
}
