console.log(factorial(5));
console.log(findMaxArr([8, 17, -5, -200, 5, 6, 7]));
console.log(findMaxRec([8, 17, -5, -200, 5, 6, 7], 0));

function factorial(n) {
  return (n===1) ? 1 : n * factorial(n-1);
}

function findMaxArr(arr) {
  let max = arr[0];
  for (const i of arr) {
    if (i > max) {
      max = i;
    }
  }
  return max;
}

function findMaxRec(arr, index) {
  if (arr.length > index) {
    let next = findMaxRec(arr, index + 1);
    return (arr[index] > next) ? arr[index] : next;
  } else {
    return arr[0];
  }
}