const students = [
  {
    id: 10,
    name: 'John Smith',
    marks: [10, 8, 6, 9, 8, 7]
  },
  {
    id: 11,
    name: 'John Doe',
    marks: [ 9, 8, 7, 6, 7]
  },
  {
    id: 12,
    name: 'Thomas Anderson',
    marks: [6, 7, 10, 8]
  },
  {
    id: 13,
    name: 'Jean-Baptiste Emanuel Zorg',
    marks: [10, 9, 8, 9]
  }
]

console.log(averageStudentMark(10));
console.log(averageGroupMark());

function averageStudentMark(id) {
  let stud = students.find(student => student.id === id);
  if (stud) {
    return average(stud.marks);
  }
  else return new Error('student not found');
}

function averageGroupMark() {
  let avgMarkArr = [];
  students.forEach(student => {
    avgMarkArr.push(averageStudentMark(student.id))
  });
  return average(avgMarkArr);
}

function average(marks) {
  return marks.reduce((a, b) => (a + b)) / marks.length;
}

// decrement(5);

function decrement(i) {
  if (i < 1) {
    return;
  }
  console.log(i);
  decrement(i-1);
}

console.log(factorial(5));

function factorial(n) {
  return (n===1) ? 1 : n * factorial(n-1);
}

console.log(max([8, 17, -5, 200, 100, 5, 6, 7]));

function max(arr) {
  let max = arr[0];
  for (const i of arr) {
    if (i > max) {
      max = i;
    }
  }
  return max;
}