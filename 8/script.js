'use strict';

function createCalculator(base) {
  return {
    add: (num) => {
      // if (isNaN(num)) {
      //   return;
      // }
      // base+=num;
      return isNaN(num) ? false : base+=num;
    },
    sub: (num) => {
      // base-=num;
      return isNaN(num) ? false : base-=num;
    },
    set: (num) => {
      // base=num;
      return isNaN(num) ? false : base=num;
    },
    get: () => base
  }
}

const calculator = createCalculator(100);

calculator.add(10); // 110 - это текущее значение base
calculator.add(10); // 120
calculator.sub(20); // 100

calculator.set(20); // 20
calculator.add(10); // 30
calculator.add(10); // 40
calculator.add('qwe'); // NaN и значение 40 не менять
console.log(calculator.get()) // 40