"use strict";

class Group {
    #students = [];

    get students() {
        return this.#students;
    }

    #isValid(value) {
        return value instanceof Student;
    }

    addStudent(student) {
        if (this.#isValid(student)) {
            this.students.push(student);
        }
    }

    getAverageMark() {
        const marks = this.students.flatMap(student => student.marks);
        return marks.reduce((a, b) => (a + b)) / marks.length;
    }
}

class Student {
    constructor(name, marks=[]) {
        this.name = name;
        this.marks = marks;
    }
}


const group = new Group();
group.addStudent(new Student('John', [10, 8]));
group.addStudent({});
group.addStudent(new Student('Alex', [10, 9]));
group.addStudent(new Student('Bob', [6, 10,]));
console.log(group.students.length);
console.log(group.getAverageMark());

//Полифил

Object.defineProperty(Array.prototype, 'max', {
    value: function () {
        let max = this[0];
        for (const i of this) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }
});

Array.prototype.max = function () {
    return Math.max(...this);
}

console.log('Max element of array ', [6, 5, 8, 7].max());