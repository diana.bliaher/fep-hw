// 'use strict';
import ContactsApi from "./ContactsApi.js";

const EDIT_BTN_CLASS = 'edit-contact';
const DELETE_BTN_CLASS = 'delete-contact';
const MODAL_SELECTOR = '#contactModal';

const addBtn = document.querySelector('#addItem');
const contactBody = document.querySelector('#contactTable tbody');
const contactTemplate = document.querySelector('#contactTemplate');

let contactList = [];

const EMPTY_NOTE = {
    id: '',
    firstName: '',
    lastName: '',
    phone: ''
};


addBtn.addEventListener('click', onAddBtnClick);
contactBody.addEventListener('click', onContactBodyClick);

ContactsApi.getList()
    .then((data) => {
        contactList = data;
        renderContactItems(data);
    })
    .catch(showError);

const $form = $(`${MODAL_SELECTOR} form`)[0];

const $modal = $(MODAL_SELECTOR).dialog({
    autoOpen: false,
    modal: true,
    show: {
        effect: "blind",
        duration: 1000
    },
    hide: {
        effect: "explode",
        duration: 1000
    },
    buttons: {
        Save: () => {
        const contact = getMessage();

        if (contact.id) {
            ContactsApi.update(contact.id, contact)
                .then(ContactsApi.getList)
                .then(renderContactItems)
                .catch(showError);
        } else {
            ContactsApi.create(contact)
            .then(newContact => {
                contactList.push(newContact);
                addContactItem(newContact);
            })
            .catch(showError);
        }

        closeModal();
        },
        Cancel: closeModal
    },
    close: closeModal
})

function openModal(contact) {
    fillForm(contact);
    $modal.dialog('open');
}

function closeModal() {
    $modal.dialog('close');
}

function onAddBtnClick(e) {
    openModal(EMPTY_NOTE);
}

function onContactBodyClick(e) {
    const contactEl = getContactItem(e.target);
    const id = contactEl.dataset.id;

    if (e.target.classList.contains(DELETE_BTN_CLASS)) {
        ContactsApi.delete(id).catch(showError);
        contactEl.remove();
        return;
    }
    if (e.target.classList.contains(EDIT_BTN_CLASS)) {
        onEditClick(id)
        return;
    }
}

function onEditClick(id) {
    const contact = contactList.find(item => item.id === id);
  
    openModal(contact);
}

function getContactItem(el) {
    return el.closest('tr');
}

function fillForm(contact) {
    $form.id.value = contact.id;
    $form.name.value = contact.firstName;
    $form.surName.value = contact.lastName;
    $form.phone.value = contact.phone;
}

function getMessage() {
    const message = {

        ...EMPTY_NOTE,
        id: $form.id.value,
        firstName: $form.name.value,
        lastName: $form.surName.value,
        phone: $form.phone.value,
    }
    return message;
}

function renderContactItems(data) {
    const html = data.map(generateContactHtml).join('');

    contactBody.innerHTML = html;
}

function addContactItem(data) {
    const contactItem = generateContactHtml(data);
    contactBody.insertAdjacentHTML('beforeend', contactItem);
}

function generateContactHtml(data) {
    return contactTemplate
        .innerHTML
        .replace('{name}', data.firstName)
        .replace('{surname}', data.lastName)
        .replace('{phone}', data.phone)
        .replace('{id}', data.id);
}

function showError(e) {
    alert(e.message);
}