"use strict";

function Hamburger(size) {
    this.size = size;
} 

Hamburger.SIZE_SMALL = {
        price: 50,
        calories: 20,
    };
Hamburger.SIZE_LARGE = {
        price: 100,
        calories: 40
    };
Hamburger.TOPPING_CHEESE = {
        price: 10,
        calories: 20
    };
Hamburger.TOPPING_POTATO = {
        price: 15,
        calories: 10
    };
Hamburger.TOPPING_MAYO = {
        price: 20,
        calories: 5
    };
Hamburger.TOPPING_SPICE = {
        price: 15,
        calories: 0
    };
    
Hamburger.prototype.addTopping = function (topping) {
    if (!("toppings" in this)) {
        this.toppings = [];
    }
    this.toppings.push(topping);

}

Hamburger.prototype.getToppings = function () {
    return this.toppings;
}

Hamburger.prototype.getSize = function () {
    return this.size;
}


Hamburger.prototype.getPrice = function () {
    var size = this.getSize();
    var price = size.price;
    
    var toppings = this.getToppings();
    toppings.forEach(function(item) {
        price += item.price;
    });
    
    return price;
}

Hamburger.prototype.getCalories = function () {    
    var size = this.getSize();
    var calories = size.calories;
    
    var toppings = this.getToppings();

    toppings.forEach(function(item) {
        calories += item.calories;
    });
    return calories;
}


var hamburger = new Hamburger(Hamburger.SIZE_SMALL);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_CHEESE);

console.log("Price with sauce: " + hamburger.getPrice());

console.log("Callories with sauce: " + hamburger.getCalories());