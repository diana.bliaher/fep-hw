'use strict';

const calc = new Calculator(100);
  
calc.add(10);
calc.add(10);
calc.sub(20);
calc.set(20);
calc.add(10);
calc.add('qwe');
console.log(calc.get());

function Calculator(base) {
    if (isNaN(base)) {
        throw new Error('Base must be a number');
    }
    this.base = base;

    this.add = function (num) {
        return isNaN(num) ? false : this.base+=num;
    }

    this.sub = function (num) {
        return isNaN(num) ? false : this.base-=num;
    }

    this.set = function (num) {
        return isNaN(num) ? false : this.base=num;
    }

    this.get = function () {
        return this.base;
    }
}