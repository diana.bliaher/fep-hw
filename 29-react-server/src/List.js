import React from "react";

export default function List({ todos, onEdit, onDelete, toggleStatusTodo }) {
  const done = todos[0].status
  function onEditClick(e, todo) {
    e.stopPropagation();

    onEdit(todo);
  }

  function onDeleteClick(e, todo) {
    e.stopPropagation();

    onDelete(todo.id);
  }

  function toggleStatusItem(e, todo) {
    e.stopPropagation();

    toggleStatusTodo(todo);
  }

  return (
    <ul id="todoList">
      {todos.map((todo, i) => (
        <li
          key={todo.id}
          className={['todo-item', (todo.status ? 'done' : '')].join(' ')} 
          onClick = {e => toggleStatusItem(e, todo)}
        >
          {todo.title}
          <button
            className="edit-button"
            onClick={e => onEditClick(e, todo)}
          >Edit</button>
          <button
            className="remove-button"
            onClick={e => onDeleteClick(e, todo)}
          >Delete</button>
        </li>
      ))}
    </ul>
  );
}