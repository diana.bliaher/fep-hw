import {useEffect, useState} from "react";
import TodoApi from "./TodoApi";

export default function useTodo(defaultList) {
  const [error, setError] = useState('');
  const [todos, setTodo] = useState(defaultList);

  function showError(e) {
    setError(e.message);
  }

  function updateTodo(newTodo) {
    return todos.map(todoItem => todoItem.id === newTodo.id ? newTodo : todoItem);
  }

  useEffect(() => {
    TodoApi.getList()
      .then(setTodo)
      .catch(showError);
  }, [])

  function onDelete(id) {
    TodoApi.delete(id)
    .then(()=>{
      const newList = todos.filter(item => item.id !== id);
      setTodo(newList);
    })
    .catch(showError);
  }

  function toggleStatusTodo(todo) {
    TodoApi.update(todo.id, {status: !todo.status})
    .then((newTodo) => {
      const newList = updateTodo(newTodo);
      setTodo(newList);
    })
    .catch(showError);
  }

  function onTodoFormSubmit(todo) {
    if (todo.id) {
      TodoApi.update(todo.id, todo)
        .then((newTodo) => {
          const newList = updateTodo(newTodo);

          setTodo(newList);
        })
        .catch(showError);
    } else {
      TodoApi.create(todo)
        .then(newTodo => {
          setTodo([...todos, newTodo]);
        })
        .catch(showError);
    }
  }

  return { error, todos, onTodoFormSubmit, onDelete, toggleStatusTodo };
}