const { src, dest, series, parallel, watch } = require('gulp');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();

function cleanDestTask() {
  return src('./dest', { read: false, allowEmpty: true }).pipe(clean());
}

function copyJs() {
  return src([
    './src/StudentsApi.js',
    './src/script.js',
  ], { sourcemaps: true  })
    .pipe(concat('script.js'))
    .pipe(uglify())
    .pipe(dest('./dest', { sourcemaps: '.'  }))
}

function copyVendorJs() {
  return src([
    './node_modules/jquery/dist/jquery.min.js',
  ])
    .pipe(concat('vendor.js'))
    .pipe(dest('./dest'))
}
 
function copyHtml() {
  return src('./src/index.html').pipe(dest('./dest'))
}

function copyCss() {
  return src('./src/style.css').pipe(dest('./dest'))
}

function serve() {
  browserSync.init({
    server: {
      baseDir: "./dest"
    }
  });

  watch(['./src/**/*.js'], { ignoreInitial: false }, series(copyJs, reloadBrowser));
  watch(['./src/*.css'], { ignoreInitial: false }, series(copyCss, reloadBrowser));
  watch(['./src/*.html'], { ignoreInitial: false }, series(copyHtml, reloadBrowser));
}

function taskBuild() {
  return series(
    cleanDestTask,
    parallel(
      copyJs,
      copyHtml,
      copyCss,
      copyVendorJs
    ),
  );
}

function taskServe() {
  return series(taskBuild(), serve);
}

function reloadBrowser(done) {
  browserSync.reload()
  done();
}

module.exports.build = taskBuild();
module.exports.serve = taskServe();