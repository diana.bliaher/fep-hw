const $studentsTable = $('#students-table');
const $addForm = $('#addForm');

const STUDENT_ITEM_SELECTOR = 'tr';
const DELETE_SELECTOR = '.del-btn';
const EDIT_SELECTOR = '.input-mark';

const EMPTY_STUDENT = {
    name: '',
    marks:["0","0","0","0","0","0","0","0","0","0"],
    id: '',
};

getTableMarks();

let studentList = [];
const $studentList = $studentsTable
  .on('click', DELETE_SELECTOR, onDeleteClick)
  .on('focusout', EDIT_SELECTOR, onInputFocusout)

$addForm.on('submit', onSubmitForm);

function onSubmitForm(e) {
    const form = $(this);
    e.preventDefault();
    let newStudent = getNewStudent();
    StudentsApi.create(newStudent)
    .then((data) => {
        renderTableItem(data);
        studentList.push(data);
        clearForm(form);
    })
}

function onDeleteClick() {
    const id = getElementId($(this));
    StudentsApi.delete(id)
    .then(() => {
        const $student = findStudentById(id);
        studentList = studentList.filter(item => item.id !== id);
  
        $student.remove();
    })
}

function getNewStudent() {
    let newName = $addForm.find('input').val();

    const student = {
        ...EMPTY_STUDENT,
        name: newName,
    }

    return student;
}

function findStudentById(id) {
    return $studentList.find(`[data-id="${id}"]`);
}

function renderTable(data) {
    const html = data.map(generateInfoHtml);
    $studentsTable.append(html);
}

function renderTableItem(data) {
    const html = generateInfoHtml(data);
    $studentsTable.append(html);
}

function onInputFocusout() {
    const $input = $(this);
    const num = $input.attr('data-number');
    const id = getElementId($input);

    const student = studentList.find(item => item.id === id);
    student.marks[num] = $input.val();

    const changes = {
        marks: student.marks,
    };
  
    updateStudent(id, changes);
}

function updateStudent(id, changes) {
    StudentsApi.update(id, changes)
    .then(() => {
        const student = studentList.find(item => item.id === id);
        Object.keys(changes).forEach(key => student[key] = changes[key]);
    })
}

function getElementId($el) {
    return $el.closest(STUDENT_ITEM_SELECTOR).attr('data-id');
}

function generateInfoHtml(data) {

    let inputArr = '';
    for (let i = 0; i < data.marks.length; i++) {
        inputArr += `<input class="input-mark" data-number="${i}" value='${data.marks[i]}'/>`
    }
    return `
        <tr class="item" data-id="${data.id}">
            <td>${data.name}</td>
            <td>${inputArr}</td>
            <td><button class="del-btn">Delete</button></td>
        </tr>
    `;
}

function getTableMarks() {
    return StudentsApi.getStudents()
        .then(function (data) {
            studentList = data;
            renderTable(data);
        })
        .catch((e) => {
            showError(e);
        });
}

function showError(e) {
    alert(e.message);
}

function clearForm(form) {
    form[0].reset();
}