const obj = {
  prop: '42',
  prop2: [8, 9, 10, {
    beautifulPropertyName: 88,
    'property with spaces': {
      a: {
        b: '',
        c: {
          someProperty: [{
            'prop name': 'I am a smart programmer',
          }],
        },
      },
    },
  }],
  prop3: function() {
    return {
      baz: 'Hello',
      bar: {
        anotherBeautifulProp: [8, {
          target: 'It was simple!',
          qwe: [8, 17, 37],
        }],
      },
    };
  },
};

console.log(obj.prop2[3]['property with spaces'].a.c.someProperty[0]['prop name'] ); // I am a smart programmer
console.log(obj.prop3().bar.anotherBeautifulProp[1].target ); // It was simple!



// з метрорської лекції
const tree = {
  value: 1,
  children: [
    {
      value: 2,
      children: [
        { value: 4 },
        { value: 5 },
      ]
    },
    {
      value: 3,
      children: [
        { value: 6 },
        { value: 7 },
      ]
    }
  ]
};
// let arr=[];
// function getTreeValues(tree) { 
//   for (const key in tree) {
//     if (Array.isArray(tree[key])) {
//       for (const i of tree[key]) {
//         if (typeof i === 'object') {
//           getTreeValues(i);
//         }
//       }
//     } else
//     arr.push(tree[key])
//   }
//   return arr;
// }
console.log(getTreeValues(tree))

function getTreeValues(tree) {
  let values = [ tree.value ];

  if (Array.isArray(tree.children)) {
    tree.children.forEach(item => values = values.concat(getTreeValues(item)));
  }

  return values;
}