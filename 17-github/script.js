'use strict';

const input = document.querySelector('#input');
const findInfoBtn = document.querySelector('#btn');
const container = document.querySelector('#container');
const userApi = 'https://api.github.com/users/{{login}}';

findInfoBtn.addEventListener('click', onFindInfoBtnClick);

function onFindInfoBtnClick(e) {
    const userName = input.value;
    if (!isValid(userName)) {
        alert('Invalid userName!');
        return;
    }
    fetch(userApi.replace('{{login}}', userName))
        .then(response => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        })
        .then(function (data) {
            renderInfo(data);
            clearInput();
        })
        .catch((error) => {
            alert(error.message)
        });
}

function renderInfo(info) {
    container.innerHTML = generateInfoHtml(info);
}

function generateInfoHtml(info) {
    return `
        <div>
            <div><img src="${info.avatar_url}"/> </div>
            <div>количество репозиториев: ${info.public_repos}</div>
            <div>количество фоловеров: ${info.followers}</div>
            <div>количество наблюдаемых: ${info.following}</div>
        <div>
    `;
}

function isValid(data) {
    return data.trim() !== '';
};

function clearInput() {
    input.value = '';
}