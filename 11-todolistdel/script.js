'use strict';

const TODO_ITEM_SELECTOR = '.todoItem';
const DELETE_BTN_CLASS = 'deleteBtn';
const GREEN_CLASS = 'green';

const input = document.querySelector('#inputItem');
const list = document.querySelector('.todoList');
const btn = document.querySelector('#insertItem');

btn.addEventListener('click', onAddBtnClick);
list.addEventListener('click', onToDoListClick);


function onAddBtnClick() {
    const message = getMessage();

    if (!isValid(message)) {
        alert('Invalid message!');
        return;
    }
    addTodoItem(message);

    clearInput();
}

function onToDoListClick(e) {
    const targetItem = getToDoItem(e.target);

    if (e.target.classList.contains(DELETE_BTN_CLASS)) {
        targetItem.remove();
        return;
    }

    targetItem.classList.toggle(GREEN_CLASS);
}

function getToDoItem(el) {
    return el.closest(TODO_ITEM_SELECTOR);
}

function getMessage() {
    return input.value;
}

function isValid(data) {
    return data.trim() !== '';
};

function addTodoItem(data) {
    const todoItemTemplateHTML = generateTodoHtml(data);
    list.insertAdjacentHTML('beforeend', todoItemTemplateHTML);
}

function generateTodoHtml(data) {
    return `
        <li class="todoItem">
            <span>${data}</span>
            <button class="deleteBtn">Delete</button>
        </li>
    `;
}

function clearInput() {
    input.value = '';
}
