'use strict';

const interview = {
  'Сколько будет 2+2?': '4',
  'Солнце встает на востоке?': true,
  'Сколько будет 5 / 0?': 'infinity',
  'Какого цвета небо?': 'голубого',
  'Как правильный ответ на «Главный вопрос жизни, вселенной и всего такого»': '42'
}

let points = checkResult() * 10;

alert(`Вы набрали ${points} очков!`);

function checkResult() {
  let res;
  let correctAnswers = 0;

  for (const key in interview) {
    if (typeof interview[key] === "boolean") {
      res = confirm(key);
    } else {
      res = prompt(key);
    }
    if (res === interview[key]) {
      correctAnswers++;
    }
  }

  return correctAnswers;
}