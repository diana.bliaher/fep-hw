const actionsArr = ['+', '-', '*', '/'];
const action = getAction();
const numberCount = getNumberCount();
const numberArr = getNumbers(numberCount);

showAnswer(action, numberArr);

function getAction() {
  let inputAction;
  do {
    inputAction = prompt(`What action do you want to do (${actionsArr.join(', ')})?`);
  } while (!actionsArr.includes(inputAction));

  return inputAction;
}

function getNumberCount() {
  let inputNumCount;
  do {
    inputNumCount = Number(prompt(`Enter the count of number:`));
  } while (isNaN(inputNumCount) || inputNumCount === '' || inputNumCount < 2 || inputNumCount > 5);

  return inputNumCount;
}

function getNumbers(count) {
  let inputNum;
  let arr = [];
  for (let i = 0; i < count; i++) {
    do {
      inputNum = Number(prompt(`Enter the number №${i+1}:`));
    } while (isNaN(inputNum) || inputNum === '');
    arr.push(inputNum);
  }

  return arr;
}

function showAnswer(action, numbers) {
  let answer = getResult(action, numbers);
  let instanceStr = '';
  // for (let i = 0; i < numbers.length; i++) {
  //   instanceStr += `${numbers[i]} `;
  //   if (i < numbers.length-1) {
  //     instanceStr += `${action} `;
  //   }
  // }
  // alert(`Answer: ${instanceStr}= ${answer}`);
  alert(`Answer: ${numberArr.join(` ${action} `)}= ${answer}`);
}

function getResult(action, numbers) {
  switch (action) {
    case '+': return add(numbers);
    case '-': return sub(numbers);
    case '*': return mult(numbers);
    case '/': return div(numbers);
  }
}


function add(numbers) {
  let count = 0;
  for (let i = 0; i < numbers.length; i++) {
    count += numbers[i];
  }
  return count;
}

function sub(numbers) {
  let count = numbers[0];
  for (let i = 1; i < numbers.length; i++) {
    count -= numbers[i];
  }
  return count;
}

function mult(numbers) {
  let count = numbers[0];
  for (let i = 1; i < numbers.length; i++) {
    count *= numbers[i];
  }
  return count;
}

function div(numbers) {
  let count = numbers[0];
  for (let i = 1; i < numbers.length; i++) {
    count /= numbers[i];
  }
  return count;
}
