class StudentsApi {
  static URL = 'https://5dd3d5ba8b5e080014dc4bfa.mockapi.io/students';

  static request(url='', method = 'GET', body) {
    return fetch(StudentsApi.URL+url, {
      method,
      body: body ? JSON.stringify(body) : undefined,
      headers: {
        'Content-type': 'application/json',
      }
    })
    .then((res) => res.json())
  }

  static getStudents() {
    return this.request();
  }

  static getOne(id) {
    return this.request(`/${id}`);
  }

  static update(id, changes) {
    return this.request(`/${id}`, 'PUT', changes);
  }

  static create(data) {
    return this.request('', 'POST', data);
  }

  static delete(id) {
    return this.request(`/${id}`, 'DELETE');
  }
}

export default StudentsApi;